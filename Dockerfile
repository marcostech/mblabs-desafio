FROM node:18

WORKDIR /app

COPY package*.json ./
COPY tsconfig.json .
COPY jest.config.ts .
COPY ./src src

# Copia envs do diretorio para dentro do container
COPY .env .
RUN npm install
RUN npm test
RUN npx tsc
ENV PORT=8080

EXPOSE 8080