# mblabs-desafio
### Objetivo:

Construir aplicação que faça uma sugestão de musica do Spotify para uma determinada faixa de temperatura, sendo cada faixa uma sugestão diferente.

### Solução:

Foi construida uma API em que o usuário apenas necessita informar uma cidade, buscamos então com esta informação, na API externa **WeatherAPI,** os dados ****de clima, passando ao sistema a temperatura da cidade é selecionado uma das três playlists disponiveis do Spotify, retornando ao usuário uma resposta contendo a playlist e informações adicionais da mesma.

### Pontos técnicos:

Para gerenciamento de rotas foi usado o express, com middleware de log das requests recebidas, que é muito útil para auditorias e debugs, o seviço de log esecolhido foi o winston, configurado apenas para salvar os logs localmente mas podendo ser facilmente escalado criando-se novos transports. A validação dos campos da request foi feita com o Joi, que é capaz de validar com muita eficiencia os campos recebidos. A documentação das rotas foi feita com swagger e está disponivel para consumo diretamente pelo backend. Os testes unitários e E2E foram feitos com Jest, conseguindo uma coberta muito proxima de 100%, o que garante as novas implementações muito mais segurança, pois a aplicação inteira está sendo testada a cada deploy e qualquer erro será observado no Jest.

A arquitetura do projeto utiliza conceitos de clean architecture, visando desacoplar e abstrair as camadas da aplicação, utilizando de interfaces para comunicação entre elas, e também utilizando o padrão de injeção de dependencia que nos traz muita flexibilidade nas implementações e mudanças pontuais, como de providers.


## Variáveis de ambiente -> env

Para rodar o projeto é necessário um arquivo contendo as seguintes variáveis de ambiente:

# Obrigatórias
- SPOTIFY_AUTH_BASE_URL -> URL do servidor de autenticação do Spotify
- SPOTIFY_BASE_URL -> URL da API Spotify
- SPOTIFY_BASIC -> Basic contendo um Base64 do clientId e clientSecret do Spotify APP criando no Spotify
- PLAYLIST_COLD -> ID da playlist do Spotify para o climate COLD
- PLAYLIST_WARM -> ID da playlist do Spotify para o climate WARM
- PLAYLIST_HOT -> ID da playlist do Spotify para o climate HOT
- WEATHER_API_KEY -> API Key do serviço WeatherAPI
- WEATHER_API_URL -> URL da API WeatherAPI
# Opcionais
- TRACKS_PER_PAGE -> Parametro para definir quantas faixas exibir por página // Opcional, default 10
- WEATHER_TIMEOUT -> Timeout das Requisições da WeatherAPI // Opcional, default 10000
- SPOTIFY_TIMEOUT -> Timeout das Requisições do Spotify // Opcional, default 10000
- COLD_CELSIUS -> Parametro para definir climate COLD // Opcional, default 10
- WARM_CELSIUS -> Parametro para definir climate WARM // Opcional, default 20
- PORT -> Parametro para definir porta de comunicação da aplicação // Opcional, default 3001

## Rodando projeto

Primeiro crie um arquivo .env com as variáveis obrigatórias na raiz do projeto, após isto rode o comando abaixo para configurar o ambiente Docker.

```
git checkout <tag name>
docker compose up
```

## Testes automatizados, unit e E2E, 100%

Durante cada deploy com o Docker, os testes serão iniciados e validados em build time.
