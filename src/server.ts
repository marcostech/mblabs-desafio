import express from 'express';
import router from './routes/index';
import cors from 'cors';
import bodyParser from 'body-parser';

const app = express();

app.use(express.json());
app.use(bodyParser.json());
app.use(cors());
app.use(router);

const port = process.env.PORT ?? 3001;
const liveApp = app.listen(port);
console.log(`App running on: ${port}`);

export default liveApp;