export const options = {
    definition: {
      openapi: '3.1.0',
      info: {
        title: 'API MBlabs Playlist',
        version: '1.0.0',
        description:
          'Sugestão de Playlists com base na temperatura de sua cidade preferida!',
        license: {
          name: 'MIT',
          url: 'https://spdx.org/licenses/MIT.html',
        },
        contact: {
          name: 'MBLABS',
          url: 'https://mblabs.com.br/',
          email: 'mblabs@mblabs.com',
        },
      },
      servers: [
        {
          url: 'http://localhost:3001',
        },
      ],
    },
    apis: ['src/routes/swagger/docs/playlist.yaml'],
  };