import { Router } from 'express';
import playlistRoutes from './playlist.routes';
import swaggerUi from 'swagger-ui-express';
import swaggerJsdoc from 'swagger-jsdoc';
import { options } from './swagger/options';

const config = swaggerJsdoc(options);
const router = Router();
router.use('/api-doc', swaggerUi.serve, swaggerUi.setup(config));
router.use('/playlist', playlistRoutes);

export default router;