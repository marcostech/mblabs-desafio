import { Router } from 'express';
import { loggerService, playlistController } from '../infra/compose/PlaylistComposer';
import { RouterAdapter } from '../infra/middlewares/RouterAdapter';
import { PlaylistSchema } from '../infra/DTO/Schemas/PlaylistSchema';
import { Validator } from '../infra/middlewares/Validator';
import { Log } from '../infra/middlewares/Log';

const playlistRoutes = Router();
const playlistSchema = new PlaylistSchema();

playlistRoutes.get('/suggestion',
    Log(loggerService),
    Validator.validate(playlistSchema.suggestion(), loggerService),
    RouterAdapter.adapt(playlistController, 'getPlaylist')
);

playlistRoutes.get('/tracks',
    Log(loggerService),
    Validator.validate(playlistSchema.tracks(), loggerService),
    RouterAdapter.adapt(playlistController, 'getTracksPaginated')
);

export default playlistRoutes;