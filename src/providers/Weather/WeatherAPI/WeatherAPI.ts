import { ILoggerService } from '../../../app/providers/ILoggerService';
import { IWeatherProvider } from '../../../app/providers/IWeatherProvider';
import { WeatherIntegrationError } from '../../../errors/BusinessExceptions/Weather/WeatherExceptions';
import { IMetadataDTO } from '../../../infra/DTO/MetadataDTO';
import { ORIGIN } from '../../../shared/utils/logger/Origin';
import { logTypes } from '../../../shared/utils/logger/logger';
import { weatherApiInstance } from './HttpInstance';
import { WeatherDTO } from './DTO/WeatherDTO';

export class WeatherAPI implements IWeatherProvider {
    constructor (
        private loggerService: ILoggerService
    ){}    
    async getWeatherByLocation(location: string, metadata: IMetadataDTO): Promise<WeatherDTO> {
        const weatherAPI = weatherApiInstance(metadata, this.loggerService);
        const { data } = await weatherAPI.get<WeatherDTO>(
            '/v1/current.json', { params: { q: location } }
            );
        this.loggerService.log(logTypes.info, {
            message: 'Retorno da busca',
            metadata,
            origin: ORIGIN.WEATHER_PROVIDER_SEARCH,
            customData: data?.current?.temp_c
        });
        if(!data) throw new WeatherIntegrationError('Empty response not acceptable');

        return data;
    }
}