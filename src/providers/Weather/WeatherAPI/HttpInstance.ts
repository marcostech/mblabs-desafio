import axios, { AxiosError } from 'axios';
import { WeatherIntegrationError } from '../../../errors/BusinessExceptions/Weather/WeatherExceptions';
import { logTypes } from '../../../shared/utils/logger/logger';
import { IMetadataDTO } from '../../../infra/DTO/MetadataDTO';
import { ILoggerService } from '../../../app/providers/ILoggerService';
import { ORIGIN } from '../../../shared/utils/logger/Origin';

export const weatherApiInstance = (metadata: IMetadataDTO, loggerService: ILoggerService) => {
    const axiosInstance = axios.create({
        baseURL: process.env.WEATHER_API_URL,
        headers: {
            'Content-Type': 'application/json'
        },
        timeout: process.env.WEATHER_TIMEOUT ? parseInt(process.env.WEATHER_TIMEOUT) : 10000,
    });

    axiosInstance.interceptors.request.use(request => {
        request.params.key = process.env.WEATHER_API_KEY;
        loggerService.log(logTypes.info, {
            message: 'Acessando WEATHER API',
            metadata,
            origin: ORIGIN.WEATHER_PROVIDER_API,
            customData: {
                baseURL: request.baseURL,
                endpoint: request.url,
            }
        });
        return request;
    });

    axiosInstance.interceptors.response.use(
        response => response,
        (error: AxiosError) => {
            loggerService.log(logTypes.error, {
                message: 'Erro acessando a API',
                metadata,
                origin: ORIGIN.WEATHER_PROVIDER_API,
                customData: {
                    baseURL: error?.config?.baseURL,
                    queryCity: error?.config?.params?.q,
                    statusCode: error?.response?.status,
                    message: error?.response?.data
                }
            });
            throw new WeatherIntegrationError(error.message);
        }
    );

    return axiosInstance;
};