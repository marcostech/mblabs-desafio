export interface WeatherDTO {
    current: {
        temp_c: number;
    };
    location: {
        localtime: Date;
        name: string
    };
}