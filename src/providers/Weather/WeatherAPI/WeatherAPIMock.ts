import { IWeatherProvider } from '../../../app/providers/IWeatherProvider';
import { WeatherIntegrationError } from '../../../errors/BusinessExceptions/Weather/WeatherExceptions';
import { IMetadataDTO } from '../../../infra/DTO/MetadataDTO';
import { WeatherDTO } from './DTO/WeatherDTO';

export class WeatherAPIMock implements IWeatherProvider {
    database: any;
    constructor(data: any) {
        this.database = data;
    }
    async getWeatherByLocation(city: string, metadata: IMetadataDTO): Promise<WeatherDTO> {
        metadata;
        const data = await this.database.locations.find(({ location }: WeatherDTO) => {
            return location.name === city;
        });
        if (data) {
            return Promise.resolve({
                current: {
                    temp_c: data.current.temp_c,
                },
                location: {
                    localtime: data.location.localtime,
                    name: data.location.name
                }
            });
        }
        throw new WeatherIntegrationError('Empty response not acceptable');
    }
}