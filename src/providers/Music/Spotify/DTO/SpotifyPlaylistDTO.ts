export interface SpotifyPlaylistDTO {
    id: string;
    name: string;
    description: string;
    external_urls: SpotifyExternalUrl
    tracks: { items: SpotifyTracks[] }
    images: SpotifyImages[]
}

interface SpotifyExternalUrl{
    spotify: string
}

interface SpotifyImages {
    height: number;
    url: string;
    width: number;
}

interface SpotifyAlbum {
    name: string;
    images: SpotifyImages[];
    external_urls: SpotifyExternalUrl
}

interface SpotifyArtists {
    external_urls: SpotifyExternalUrl
    name: string;
}

export interface SpotifyTracks {
    track: {
        album: SpotifyAlbum ;
        artists: SpotifyArtists[];
        duration_ms: number;
        external_urls: SpotifyExternalUrl
        name: string;
        total: number;
        id: string;
    }
}

export interface SpotifyTracksDTO {
    items: SpotifyTracks[],
    total: number;
    href: string;
}
