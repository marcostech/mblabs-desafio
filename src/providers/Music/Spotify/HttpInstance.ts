import axios, { AxiosError } from 'axios';
import { PlaylistIntegrationError } from '../../../errors/BusinessExceptions/Playlist/PlaylistExceptions';
import { logTypes } from '../../../shared/utils/logger/logger';
import { IMetadataDTO } from '../../../infra/DTO/MetadataDTO';
import { ILoggerService } from '../../../app/providers/ILoggerService';
import { ORIGIN } from '../../../shared/utils/logger/Origin';

export const authInstance = (metadata: IMetadataDTO, loggerService: ILoggerService) => {
    const axiosInstance = axios.create({
        baseURL: process.env.SPOTIFY_AUTH_BASE_URL,
        headers: {
            Authorization: `Basic ${process.env.SPOTIFY_BASIC}`,
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        timeout: process.env.SPOTIFY_TIMEOUT ? parseInt(process.env.SPOTIFY_TIMEOUT) : 10000,
    });

    axiosInstance.interceptors.request.use(request => {
        loggerService.log(logTypes.info, {
            message: 'Authenticando no SPOTIFY API',
            metadata,
            origin: ORIGIN.MUSIC_PROVIDER_AUTH,
            customData: {
                baseURL: request.baseURL
            }
        });
        return request;
    });

    axiosInstance.interceptors.response.use(
        response => response,
        error => {
            loggerService.log(logTypes.error, {
                message: 'Erro na authenticação',
                metadata,
                origin: ORIGIN.MUSIC_PROVIDER_AUTH,
                customData: {
                    baseURL: error?.config?.baseURL,
                    statusCode: error?.response?.status,
                    message: error?.response?.data
                }
            });
            throw new PlaylistIntegrationError(error.message);
        }
    );

    return axiosInstance;
};

export const spotifyInstance = (token: string, metadata: IMetadataDTO, loggerService: ILoggerService) => {
    const axiosInstance = axios.create({
        baseURL: process.env.SPOTIFY_BASE_URL,
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
        timeout: process.env.SPOTIFY_TIMEOUT ? parseInt(process.env.SPOTIFY_TIMEOUT) : 10000,
    });
    
    axiosInstance.interceptors.request.use(request => {
        loggerService.log(logTypes.info, {
            message: 'Acessando SPOTIFY API',
            metadata,
            origin: ORIGIN.MUSIC_PROVIDER_API,
            customData: {
                baseURL: request.baseURL,
                endpoint: request.url,
            }
        });
        return request;
    });

    axiosInstance.interceptors.response.use(
        response => response,
        (error: AxiosError) => {
            loggerService.log(logTypes.error, {
                message: 'Erro acessando a API',
                metadata,
                origin: ORIGIN.MUSIC_PROVIDER_API,
                customData: {
                    baseURL: error?.config?.baseURL,
                    statusCode: error?.response?.status,
                    message: error?.response?.data
                }
            });
            throw new PlaylistIntegrationError(error.message);
        }
    );

    return axiosInstance;
};