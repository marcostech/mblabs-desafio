import { IMusicProvider } from '../../../app/providers/IMusicProvider';
import 'dotenv/config';
import { authInstance, spotifyInstance } from './HttpInstance';
import { SpotifyAuthDTO } from './DTO/SpotifyAuthDTO';
import { SpotifyPlaylistDTO, SpotifyTracksDTO } from './DTO/SpotifyPlaylistDTO';
import { Tracks } from '../../../app/domain/Track';
import { Artists } from '../../../app/domain/Artists';
import { ImageRef } from '../../../app/domain/ImageRef';
import { Album } from '../../../app/domain/Album';
import { Playlist } from '../../../app/domain/Playlist';
import { PlaylistDetails } from '../../../app/domain/PlaylistDetails';
import { millisecondsToTime } from '../../../shared/utils/utils';
import { PlaylistIntegrationError } from '../../../errors/BusinessExceptions/Playlist/PlaylistExceptions';
import { ILoggerService } from '../../../app/providers/ILoggerService';
import { IMetadataDTO } from '../../../infra/DTO/MetadataDTO';
import { ORIGIN } from '../../../shared/utils/logger/Origin';
import { logTypes } from '../../../shared/utils/logger/logger';

export class Spotify implements IMusicProvider {
    private accessToken: string;

    constructor(
        private loggerService: ILoggerService
    ) {
        this.accessToken = '';
    }
    coldPlaylistId(): string {
        return process.env.PLAYLIST_COLD || '';
    }
    warmPlaylistId(): string {
        return process.env.PLAYLIST_WARM || '';
    }
    hotPlaylistId(): string {
        return process.env.PLAYLIST_HOT || '';
    }

    async authorizer(metadata: IMetadataDTO) {
        const authClient = authInstance(metadata, this.loggerService);
        const { data } = await authClient.post<SpotifyAuthDTO>('/api/token',
            {
                grant_type: 'client_credentials'
            });
        this.loggerService.log(logTypes.info, {
            message: 'Retorno da authenticação',
            metadata,
            origin: ORIGIN.MUSIC_PROVIDER_AUTH
        });
        if (!data.access_token) throw new PlaylistIntegrationError('Authorization error');
        this.accessToken = data.access_token;
    }

    async getPlaylistDetailById(id: string, metadata: IMetadataDTO): Promise<PlaylistDetails> {
        this.loggerService.log(logTypes.info, {
            message: 'Authenticando',
            metadata,
            origin: ORIGIN.MUSIC_PROVIDER_PLAYLIST_SEARCH
        });
        await this.authorizer(metadata);
        const spotifyApp = spotifyInstance(this.accessToken, metadata, this.loggerService);
        const { data } = await spotifyApp.get<SpotifyPlaylistDTO>(`/v1/playlists/${id}?market=BR`);
        this.loggerService.log(logTypes.info, {
            message: 'Retorno da busca',
            metadata,
            origin: ORIGIN.MUSIC_PROVIDER_PLAYLIST_SEARCH,
            customData: data?.id
        });
        if (!data) throw new PlaylistIntegrationError('Empty response not acceptable');

        return this.spotifyPlaylistResponseMapper(data);
    }

    async getPlaylistTracksById(id: string, metadata: IMetadataDTO, page: number = 1): Promise<Playlist> {
        const pageItemsMax = process.env.TRACKS_PER_PAGE ? parseInt(process.env.TRACKS_PER_PAGE) : 10;
        const offsetItems = Math.floor(page * pageItemsMax) ?? 0;
        this.loggerService.log(logTypes.info, {
            message: 'Authenticando',
            metadata,
            origin: ORIGIN.MUSIC_PROVIDER_TRACKS_SEARCH
        });
        await this.authorizer(metadata);
        const spotifyApp = spotifyInstance(this.accessToken, metadata, this.loggerService);
        const { data } = await spotifyApp.get<SpotifyTracksDTO>(
            `/v1/playlists/${id}/tracks?limit=${pageItemsMax}&offset=${offsetItems}&market=BR`
        );
        this.loggerService.log(logTypes.info, {
            message: 'Retorno da busca',
            metadata,
            origin: ORIGIN.MUSIC_PROVIDER_TRACKS_SEARCH,
            customData: data?.total
        });
        if (!data) throw new PlaylistIntegrationError('Empty response not acceptable');

        return this.spotifyTracksResponseMapper(data);
    }

    spotifyPlaylistResponseMapper(playlist: SpotifyPlaylistDTO): PlaylistDetails {
        const playlistImages: ImageRef[] = [];
        playlist.images.map((image) => {
            playlistImages.push(new ImageRef({
                url: image.url
            }));
        });
        return new PlaylistDetails({
            name: playlist.name,
            description: playlist.description,
            url: playlist.external_urls.spotify,
            image: playlistImages,
            playlistId: playlist.id
        });
    }

    spotifyTracksResponseMapper(playlistTracks: SpotifyTracksDTO): Playlist {
        const tracks: Tracks[] = [];
        playlistTracks.items.map((item) => {
            const artists: Artists[] = [];
            item.track.artists.map((artist) => {
                artists.push(new Artists({
                    name: artist.name,
                    link: artist.external_urls.spotify
                }));
            });
            const albumImages: ImageRef[] = [];
            item.track.album.images.map((image) => {
                albumImages.push(image);
            });
            const album: Album = {
                name: item.track.album.name,
                link: item.track.album.external_urls.spotify,
                images: albumImages,
            };
            const durationFormated = millisecondsToTime(item.track.duration_ms);
            this.loggerService.log(logTypes.info, {
                metadata: {
                    traceid: '1'
                },
                origin: ORIGIN.INCOMING_REQUEST,
                message: '',
                customData:
                {
                    name: item.track.name,
                    album: album,
                    duration: durationFormated,
                    link: item.track.external_urls.spotify,
                    artists: artists
                }});
            
            tracks.push(new Tracks({
                name: item.track.name,
                album: album,
                duration: durationFormated,
                link: item.track.external_urls.spotify,
                artists: artists
            }));
        });
        return new Playlist({
            tracks,
            totalTracks: playlistTracks.total
        });
    }
}