import { Album } from '../../../app/domain/Album';
import { Artists } from '../../../app/domain/Artists';
import { ImageRef } from '../../../app/domain/ImageRef';
import { Playlist } from '../../../app/domain/Playlist';
import { PlaylistDetails } from '../../../app/domain/PlaylistDetails';
import { Tracks } from '../../../app/domain/Track';
import { IMusicProvider } from '../../../app/providers/IMusicProvider';
import { PlaylistIntegrationError } from '../../../errors/BusinessExceptions/Playlist/PlaylistExceptions';
import { IMetadataDTO } from '../../../infra/DTO/MetadataDTO';
import { millisecondsToTime } from '../../../shared/utils/utils';
import { SpotifyPlaylistDTO, SpotifyTracksDTO } from './DTO/SpotifyPlaylistDTO';

export class SpotifyMock implements IMusicProvider {
    database: any;
    constructor(data: any) {
        this.database = data;
    }
    getPlaylistDetailById(playlsitId: string, metadata: IMetadataDTO): Promise<PlaylistDetails> {
        metadata;
        const data = this.database.playlists.find(({ id }: SpotifyPlaylistDTO) => {
            return playlsitId === id;
        });
        if (data) {
            return Promise.resolve(this.spotifyPlaylistResponseMapper(data));
        }
        throw new PlaylistIntegrationError('Empty response not acceptable');
    }
    async getPlaylistTracksById(playlistId: string, metadata: IMetadataDTO, page?: number | undefined): Promise<Playlist> {
        metadata;
        page;
        const data = this.database.playlistTracks.find(({ href }: SpotifyTracksDTO) => {
            return href.includes(playlistId);
        });
        if (data) {
            return Promise.resolve(this.spotifyTracksResponseMapper(data));
        }
        throw new PlaylistIntegrationError('Empty response not acceptable');
    }
    coldPlaylistId(): string {
        return '37i9dQZF1DX8mBRYewE6or';
    }
    warmPlaylistId(): string {
        return '37i9dQZF1DX8mBRYewE6or';
    }
    hotPlaylistId(): string {
        return '37i9dQZF1DX8mBRYewE6or';
    }

    spotifyPlaylistResponseMapper(playlist: SpotifyPlaylistDTO): PlaylistDetails {
        const playlistImages: ImageRef[] = [];
        playlist.images.map((image) => {
            playlistImages.push(new ImageRef({
                url: image.url
            }));
        });
        return new PlaylistDetails({
            name: playlist.name,
            description: playlist.description,
            url: playlist.external_urls.spotify,
            image: playlistImages,
            playlistId: playlist.id
        });
    }

    spotifyTracksResponseMapper(playlistTracks: SpotifyTracksDTO): Playlist {
        const tracks: Tracks[] = [];
        playlistTracks.items.map((item) => {
            const artists: Artists[] = [];
            item.track.artists.map((artist) => {
                artists.push(new Artists({
                    name: artist.name,
                    link: artist.external_urls.spotify
                }));
            });
            const albumImages: ImageRef[] = [];
            item.track.album.images.map((image) => {
                albumImages.push(image);
            });
            const album: Album = {
                name: item.track.album.name,
                link: item.track.album.external_urls.spotify,
                images: albumImages,
            };
            const durationFormated = millisecondsToTime(item.track.duration_ms);
            tracks.push(new Tracks({
                name: item.track.name,
                album: album,
                duration: durationFormated,
                link: item.track.external_urls.spotify,
                artists: artists
            }));
        });
        return new Playlist({
            tracks,
            totalTracks: playlistTracks.total
        });
    }
}