import { Tracks } from './Track';

export class Playlist {
    readonly tracks: Tracks[];
    readonly totalTracks: number;
    
    constructor(playlist: Playlist){
        this.tracks = playlist.tracks;
        this.totalTracks = playlist.totalTracks;
    }
}