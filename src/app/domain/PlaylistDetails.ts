import { ImageRef } from './ImageRef';

export class PlaylistDetails {
    readonly name: string;
    readonly description: string;
    readonly url: string;
    readonly image: ImageRef[];
    readonly playlistId: string;

    constructor(playlistDetaisl: PlaylistDetails){
        this.name = playlistDetaisl.name;
        this.description = playlistDetaisl.description;
        this.url = playlistDetaisl.url;
        this.image = playlistDetaisl.image;
        this.playlistId = playlistDetaisl.playlistId;
    }
}