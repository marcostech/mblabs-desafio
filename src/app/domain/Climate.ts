export const Climate = {
    COLD: 'COLD',
    WARM: 'WARM',
    HOT: 'HOT'
};