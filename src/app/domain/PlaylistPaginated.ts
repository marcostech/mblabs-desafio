import { Playlist } from './Playlist';

export class PlaylistPaginated {
    readonly totalPages: number;
    readonly totalTracks: number;
    readonly playlistTracks: Omit<Playlist, 'totalTracks'>;
    
    constructor(playlistPaginated: PlaylistPaginated){
        this.totalPages = playlistPaginated.totalPages;
        this.playlistTracks = playlistPaginated.playlistTracks;
        this.totalTracks = playlistPaginated.totalTracks;
    }
}