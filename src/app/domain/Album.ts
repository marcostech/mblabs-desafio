import { ImageRef } from './ImageRef';

export class Album {
    readonly name: string;
    readonly images: ImageRef[];
    readonly link: string;
    

    constructor(album: Album) {
        this.images = album.images;
        this.name = album.name;
        this.link = album.link;
    }
}