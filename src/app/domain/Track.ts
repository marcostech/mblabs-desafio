import { Album } from './Album';
import { Artists } from './Artists';

export class Tracks {
    readonly album: Album;
    readonly artists: Artists[];
    readonly duration: string;
    readonly link: string;
    readonly name: string;
    
    constructor(track: Tracks) {
        this.album = track.album;
        this.artists = track.artists;
        this.duration = track.duration;
        this.link = track.link;
        this.name = track.name;
    }
}