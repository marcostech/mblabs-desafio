export class Artists {
    readonly name: string;
    readonly link: string;

    constructor(artist: Artists) {
        this.name = artist.name;
        this.link = artist.link;
    }
}