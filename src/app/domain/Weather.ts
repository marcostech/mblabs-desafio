export class Weather {
    readonly temperature: number;
    readonly city: string;
    readonly time: Date;

    constructor(weather: Weather) {
        this.temperature = weather.temperature;
        this.city = weather.city;
        this.time = weather.time;
    }
}