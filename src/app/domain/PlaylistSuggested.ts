import { Playlist } from './Playlist';
import { PlaylistDetails } from './PlaylistDetails';

export class PlaylistSuggested {
    readonly playlistDetails: PlaylistDetails;
    readonly playlistTracks: Playlist;
    
    constructor(playlistSuggested: PlaylistSuggested) {
        this.playlistDetails = playlistSuggested.playlistDetails;
        this.playlistTracks = playlistSuggested.playlistTracks;
    }
}