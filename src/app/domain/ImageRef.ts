export class ImageRef {
    readonly height?: number;
    readonly width?: number;
    readonly url: string;

    constructor(image: ImageRef) {
        this.height = image?.height;
        this.width = image?.width;
        this.url = image.url;
    }
}