import { IMetadataDTO } from '../../../infra/DTO/MetadataDTO';
import { ORIGIN } from '../../../shared/utils/logger/Origin';
import { logTypes } from '../../../shared/utils/logger/logger';
import { ILoggerService } from '../../providers/ILoggerService';
import { IWeatherProvider } from '../../providers/IWeatherProvider';
import { Weather } from '../../domain/Weather';
import { IWeaterUseCase } from './IWeatherUseCase';

export class WeatherUseCase implements IWeaterUseCase {
    constructor(
        private weatherProvider: IWeatherProvider,
        private loggerService: ILoggerService
    ) { }
    async getTemperature(location: string, metadata: IMetadataDTO): Promise<Weather> {
        this.loggerService.log(logTypes.info, {
            message: `Buscando temperatura da cidade: ${location}`,
            metadata,
            origin: ORIGIN.WEATHER_SEARCH
        });
        const weather = await this.weatherProvider.getWeatherByLocation(location, metadata);
        this.loggerService.log(logTypes.info, {
            message: `Temperatura da cidade ${location}: ${weather.current.temp_c}`,
            metadata,
            origin: ORIGIN.WEATHER_SEARCH,
            customData: {
                city: weather.location.name,
                celsiusTemp: weather.current.temp_c
            }
        });
        return new Weather({
            temperature: weather.current.temp_c,
            city: weather.location.name,
            time: weather.location.localtime
        });
    }

    getClimate(temperature: number, metadata: IMetadataDTO): string {
        this.loggerService.log(logTypes.info, {
            message: `Buscando clima: ${temperature} °C`,
            metadata,
            origin: ORIGIN.WEATHER_CLIMATE
        });
        let climate: string;
        const coldCelsiusParam = process.env.COLD_CELSIUS ? parseInt(process.env.COLD_CELSIUS) : 10;
        const warmCelsiusParam = process.env.WARM_CELSIUS ? parseInt(process.env.WARM_CELSIUS) : 20;
        if (temperature <= coldCelsiusParam) {
            climate = 'COLD';
        } else if (temperature >= coldCelsiusParam && temperature <= warmCelsiusParam) {
            climate = 'WARM';
        } else {
            climate = 'HOT';
        }

        this.loggerService.log(logTypes.info, {
            message: `Clima definido: ${climate}`,
            metadata,
            origin: ORIGIN.WEATHER_CLIMATE,
            customData: {
                coldParameter: coldCelsiusParam,
                warmParameter: warmCelsiusParam,
            }
        });
        return climate;
    }

}