import { IMetadataDTO } from '../../../infra/DTO/MetadataDTO';
import { Weather } from '../../domain/Weather';

export interface IWeaterUseCase {
    getTemperature(location: string, metadata: IMetadataDTO): Promise<Weather>
    getClimate(temperature: number, metadata: IMetadataDTO): string
}