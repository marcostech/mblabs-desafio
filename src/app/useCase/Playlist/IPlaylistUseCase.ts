import { IMetadataDTO } from '../../../infra/DTO/MetadataDTO';
import { Playlist } from '../../domain/Playlist';
import { PlaylistDetails } from '../../domain/PlaylistDetails';
import { PlaylistPaginated } from '../../domain/PlaylistPaginated';
import { PlaylistSuggested } from '../../domain/PlaylistSuggested';

export interface IPlaylistUseCase {
    getPlaylistDetailsById(id: string, metadata: IMetadataDTO): Promise<PlaylistDetails>;
    getSuggestedPlaylistIdByTemp(climate: string, metadata: IMetadataDTO): string;
    getPlaylistTracksById(id: string, metadata: IMetadataDTO, page?: number): Promise<Playlist>;
    suggestPlaylist(climate: string, metadata: IMetadataDTO): Promise<PlaylistSuggested>;
    getPlaylistPaginated(playlistId: string, metadata: IMetadataDTO, page?: number): Promise<PlaylistPaginated>
}