import { PlaylistClimateError } from '../../../errors/BusinessExceptions/Playlist/PlaylistExceptions';
import { IMetadataDTO } from '../../../infra/DTO/MetadataDTO';
import { ORIGIN } from '../../../shared/utils/logger/Origin';
import { logTypes } from '../../../shared/utils/logger/logger';
import { ILoggerService } from '../../providers/ILoggerService';
import { IMusicProvider } from '../../providers/IMusicProvider';
import { Climate } from '../../domain/Climate';
import { Playlist } from '../../domain/Playlist';
import { PlaylistDetails } from '../../domain/PlaylistDetails';
import { PlaylistPaginated } from '../../domain/PlaylistPaginated';
import { PlaylistSuggested } from '../../domain/PlaylistSuggested';
import { IPlaylistUseCase } from './IPlaylistUseCase';

export class PlaylistUseCase implements IPlaylistUseCase {
    constructor(
        private musicProvider: IMusicProvider,
        private loggerService: ILoggerService
    ) { }
    getSuggestedPlaylistIdByTemp(climate: string, metadata: IMetadataDTO): string {
        this.loggerService.log(logTypes.info, {
            message: `Buscando id da Playlist baseado na temperatura: ${climate}`,
            metadata,
            origin: ORIGIN.PLAYLIST_SEARCH
        });
        let playlistId;
        switch (climate) {
            case Climate.COLD:
                playlistId = this.musicProvider.coldPlaylistId();
                break;
            case Climate.WARM:
                playlistId = this.musicProvider.warmPlaylistId();
                break;
            case Climate.HOT:
                playlistId = this.musicProvider.hotPlaylistId();
                break;
            default:
                playlistId = '';
        }
        this.loggerService.log(logTypes.info, {
            message: `Id selecionado: ${playlistId}`,
            metadata,
            origin: ORIGIN.PLAYLIST_SEARCH
        });
        if (!playlistId) throw new PlaylistClimateError('Failed to recognize weather climate');
        return playlistId;
    }

    async getPlaylistDetailsById(id: string, metadata: IMetadataDTO): Promise<PlaylistDetails> {
        this.loggerService.log(logTypes.info, {
            message: `Buscando detalhes da playlist id: ${id}`,
            metadata,
            origin: ORIGIN.PLAYLIST_SEARCH
        });
        return await this.musicProvider.getPlaylistDetailById(id, metadata);        
    }

    async getPlaylistTracksById(id: string, metadata: IMetadataDTO, page?: number): Promise<Playlist> {
        this.loggerService.log(logTypes.info, {
            message: `Buscando detalhes da playlist id: ${id}`,
            metadata,
            origin: ORIGIN.PLAYLIST_TRACKS_SEARCH
        });
        return await this.musicProvider.getPlaylistTracksById(id, metadata, page);        
    }

    async suggestPlaylist(climate: string, metadata: IMetadataDTO): Promise<PlaylistSuggested> {
        this.loggerService.log(logTypes.info, {
            message: 'Buscando playlist',
            metadata,
            origin: ORIGIN.PLAYLIST_SUGGESTION
        });
        const playlistId = this.getSuggestedPlaylistIdByTemp(climate, metadata);
        const playlistDetails = await this.getPlaylistDetailsById(playlistId, metadata);
        const playlistTracks = await this.getPlaylistTracksById(playlistId, metadata);
        this.loggerService.log(logTypes.info, {
            message: `Playlist Sugerida: ${playlistId}`,
            metadata,
            origin: ORIGIN.PLAYLIST_SUGGESTION
        });
        return new PlaylistSuggested({
            playlistDetails: playlistDetails,
            playlistTracks: playlistTracks
        });
    }

    async getPlaylistPaginated(playlistId: string, metadata: IMetadataDTO, page?: number): Promise<PlaylistPaginated> {
        this.loggerService.log(logTypes.info, {
            message: 'Buscando Tracks',
            metadata,
            origin: ORIGIN.PLAYLIST_TRACKS_SEARCH,
            customData: {
                page: page,
                playlistId: playlistId
            }
        });
        const tracksPerPage = process.env.TRACKS_PER_PAGE ? parseInt(process.env.TRACKS_PER_PAGE) : 10;
        const { totalTracks, ...playlistTracks } = await this.getPlaylistTracksById(playlistId, metadata, page);
        const totalPages = Math.round(totalTracks / tracksPerPage);
        this.loggerService.log(logTypes.info, {
            message: `Tracks encontradas: ${totalTracks}`,
            metadata,
            origin: ORIGIN.PLAYLIST_PAGINATED_TRACKS,
            customData: {
                totalPages,
                page
            }
        });
        
        return new PlaylistPaginated({
            playlistTracks,
            totalPages: totalPages,
            totalTracks: totalTracks
        });
    }
}