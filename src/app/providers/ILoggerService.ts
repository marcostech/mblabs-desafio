import { IMetadataDTO } from '../../infra/DTO/MetadataDTO';

export interface ILoggerService {
    log(level: string, message: ILoggerMessage): void;
}
export interface ILoggerMessage {
    message: string;
    metadata: IMetadataDTO,
    origin: string,
    customData?: any
}