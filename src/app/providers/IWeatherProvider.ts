import { IMetadataDTO } from '../../infra/DTO/MetadataDTO';
import { WeatherDTO } from '../../providers/Weather/WeatherAPI/DTO/WeatherDTO';

export interface IWeatherProvider {
    getWeatherByLocation(location: string, metadata: IMetadataDTO): Promise<WeatherDTO>
}