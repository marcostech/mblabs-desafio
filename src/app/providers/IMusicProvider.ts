import { IMetadataDTO } from '../../infra/DTO/MetadataDTO';
import { Playlist } from '../domain/Playlist';
import { PlaylistDetails } from '../domain/PlaylistDetails';

export interface IMusicProvider {
    getPlaylistDetailById(id: string, metadata: IMetadataDTO): Promise<PlaylistDetails>
    getPlaylistTracksById(id: string, metadata: IMetadataDTO, page?: number): Promise<Playlist>
    coldPlaylistId(): string
    warmPlaylistId(): string
    hotPlaylistId(): string
}