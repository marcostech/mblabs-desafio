export class ServerError extends Error {
    constructor(message?: string){
        super();
        this.name = 'SERVER_ERROR';
        this.message = message ?? 'Server Unknown Error';
    }
}
export class InvalidParameters extends Error {
    constructor(message?: string){
        super();
        this.name = 'INVALID_PARAMETERS';
        this.message = message ?? 'Invalid parameters';
    }
}