export class PlaylistIntegrationError extends Error {
    constructor(message?: string){
        super();
        this.name = 'PLAYLIST_INTEGRATION_ERROR';
        this.message = message ?? 'Playlist API Unknown Error';
    }
}
export class PlaylistClimateError extends Error {
    constructor(message?: string){
        super();
        this.name = 'PLAYLIST_CLIMATE_ERROR';
        this.message = message ?? 'Playlist API Unknown Error';
    }
}