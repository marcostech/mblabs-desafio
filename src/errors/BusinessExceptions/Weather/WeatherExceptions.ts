export class WeatherIntegrationError extends Error {
    constructor(message?: string){
        super();
        this.name = 'WEATHER_INTEGRATION_ERROR';
        this.message = message ?? 'Weather API Unknown Error';
    }
}