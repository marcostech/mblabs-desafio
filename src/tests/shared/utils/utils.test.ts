
import { millisecondsToTime } from '../../../shared/utils/utils';

describe('Function - ms to formated time', () => {
    it('utils test', () => {
        const time = millisecondsToTime(5000);
        expect(time).toBe('00:00:05');
    });
});