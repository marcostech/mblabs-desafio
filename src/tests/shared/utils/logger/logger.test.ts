import winston from 'winston';
import { Logger } from '../../../../shared/utils/logger/logger';
import { ILoggerMessage } from '../../../../app/providers/ILoggerService';

describe('Service - Logger', () => {
    it('Instance', async () => {
        const mock: ILoggerMessage ={
            message: 'test',
            metadata: {
                traceid: 'teste-trace-id',
            },
            origin: 'TEST',
        };
        
        const logger = new Logger();
        await logger.log('info', mock);
        expect(logger).toBeInstanceOf(Logger);
        expect(typeof logger.log).toBe('function');
        expect(logger.loggerInstance).toBeInstanceOf(winston.Logger);
    });
});