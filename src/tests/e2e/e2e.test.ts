import supertest, { SuperAgentTest } from 'supertest';
import liveApp from '../../server';
import 'dotenv/config';

describe('Playlist Suggestion', () => {
    const env = process.env;
    let appServer: SuperAgentTest;
    beforeEach(async () => {
        process.env = { ...env };
        appServer = supertest.agent(liveApp);
    });
    afterEach(async () => {
        process.env = env;
        liveApp.close();
    });

    describe('E2E server test', () => {
        let playlistId = '37i9dQZF1DX6e81LupkkgG';
        let location = 'Oymyakon';
        it('/playlist/suggestion -Success - Cold city', async () => {
            location = 'Oymyakon';
            const res = await appServer.get(`/playlist/suggestion?location=${location}`);
            expect(res.statusCode).toBe(200);
        });

        it('/playlist/suggestion - Port Change', async () => {
            process.env.PORT = '3001';
            location = 'Dallol';
            const res = await appServer.get(`/playlist/suggestion?location=${location}`);
            expect(res.statusCode).toBe(200);
        });
        
        it('/playlist/suggestion - Success - Hot city', async () => {
            process.env.COLD_CELSIUS = '1';
            process.env.WARM_CELSIUS = '5';
            location = 'Dallol'; // HOT city
            process.env.WEATHER_TIMEOUT = '';
            const res = await appServer.get(`/playlist/suggestion?location=${location}`);
            expect(res.statusCode).toBe(200);
        });
        
        it('/playlist/suggestion - Success - Warm city', async () => {
            process.env.COLD_CELSIUS = '1';
            process.env.WARM_CELSIUS = '50';
            location = 'London'; // Warm city
            const res = await appServer.get(`/playlist/suggestion?location=${location}`);
            expect(res.statusCode).toBe(200);
        });
        it('/playlist/suggestion - Error - no location', async () => {
            const res = await appServer.get('/playlist/suggestion?location=');
            expect(res.statusCode).toBe(400);
            expect(res.body.body.error.erro).toBe('INVALID_PARAMETERS');
        });
        it('/playlist/suggestion - Error - location not found', async () => {
            const res = await appServer.get('/playlist/suggestion?location=123123');
            expect(res.statusCode).toBe(400);
            expect(res.body.error.erro).toBe('WEATHER_INTEGRATION_ERROR');
        });
        it('/playlist/tracks - Success - tracks', async () => {
            playlistId = '37i9dQZF1DX6e81LupkkgG';
            process.env.SPOTIFY_TIMEOUT = '';
            const res = await appServer.get(`/playlist/tracks?playlistId=${playlistId}`);
            expect(res.statusCode).toBe(200);
        });
        it('/playlist/tracks - Success - tracks page 2', async () => {
            playlistId = '37i9dQZF1DX6e81LupkkgG';
            process.env.TRACKS_PER_PAGE = '10';
            const page = '2';
            const res = await appServer.get(`/playlist/tracks?playlistId=${playlistId}&page=${page}`);
            expect(res.statusCode).toBe(200);
        });
        it('/playlist/tracks - Error - tracks page 0', async () => {
            playlistId = '37i9dQZF1DX6e81LupkkgG';
            process.env.TRACKS_PER_PAGE = '0';
            const page = '0';
            const res = await appServer.get(`/playlist/tracks?playlistId=${playlistId}&page=${page}`);
            expect(res.statusCode).toBe(400);
            expect(res.body.error.erro).toBe('PLAYLIST_INTEGRATION_ERROR');
        });
        it('/playlist/tracks - Error - tracks', async () => {
            playlistId = '123123';
            const res = await appServer.get(`/playlist/tracks?playlistId=${playlistId}`);
            expect(res.statusCode).toBe(400);
        });
        it('/playlist/tracks - Error - authorization', async () => {
            process.env.SPOTIFY_BASIC = '123123';
            process.env.SPOTIFY_TIMEOUT = '';
            playlistId = '37i9dQZF1DX6e81LupkkgG';
            const res = await appServer.get(`/playlist/tracks?playlistId=${playlistId}`);
            expect(res.statusCode).toBe(400);
        });
        it('/playlist/suggestion - Error - empty playlist env COLD', async () => {
            process.env.PLAYLIST_COLD ='';
            process.env.COLD_CELSIUS = '50';
            process.env.WARM_CELSIUS = '55';
            location = 'Oymyakon';
            const res = await appServer.get(`/playlist/suggestion?location=${location}`);
            expect(res.statusCode).toBe(400);
        });
        it('/playlist/suggestion - Error - empty playlist env WARM', async () => {
            process.env.PLAYLIST_WARM ='';
            process.env.COLD_CELSIUS = '1';
            process.env.WARM_CELSIUS = '50';
            location = 'London';
            const res = await appServer.get(`/playlist/suggestion?location=${location}`);
            expect(res.statusCode).toBe(400);
        });
        it('/playlist/suggestion - Error - empty playlist env HOT', async () => {
            process.env.PLAYLIST_HOT ='';
            process.env.COLD_CELSIUS = '1';
            process.env.WARM_CELSIUS = '5';
            location = 'Dallol';
            const res = await appServer.get(`/playlist/suggestion?location=${location}`);
            expect(res.statusCode).toBe(400);
        });
    });
});