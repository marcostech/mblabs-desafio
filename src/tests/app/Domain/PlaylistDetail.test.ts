import { PlaylistDetails } from '../../../app/domain/PlaylistDetails';
import { playlistDetails } from '../../fixtures/playlistSuggested.json';


describe('Domain - PlaylistDetails', () => {
    it('Instance', () => {
        const playlist = new PlaylistDetails(playlistDetails);
        expect(playlist).toBeInstanceOf(PlaylistDetails);
        expect(playlist.name).toBe(playlistDetails.name);
        expect(playlist.url).toBe(playlistDetails.url);
        expect(playlist.description).toBe(playlistDetails.description);
        expect(playlist.playlistId).toBe(playlistDetails.playlistId);        
        expect(playlist.image).toBe(playlistDetails.image);
    });
});