import { PlaylistPaginated } from '../../../app/domain/PlaylistPaginated';
import  playlistTracks  from '../../fixtures/playlistPaginated.json';

describe('Domain - PlaylistPaginated', () => {
    it('Instance', () => {
        const playlistPaginatedEnt = new PlaylistPaginated({
            totalTracks: playlistTracks.totalTracks,
            totalPages: playlistTracks.totalPages,
            playlistTracks: playlistTracks.playlistTracks
        });
        expect(playlistPaginatedEnt).toBeInstanceOf(PlaylistPaginated);
        expect(playlistPaginatedEnt.totalPages).toBe(playlistTracks.totalPages);
        expect(playlistPaginatedEnt.totalTracks).toBe(playlistTracks.totalTracks);
        expect(playlistPaginatedEnt.playlistTracks).toBe(playlistTracks.playlistTracks);
    });
});