import { Climate } from '../../../app/domain/Climate';

describe('Domain - Climate', () => {
    it('Enum', () => {
        expect(Climate.COLD).toBe('COLD');
        expect(Climate.WARM).toBe('WARM');
        expect(Climate.HOT).toBe('HOT');
    });
});