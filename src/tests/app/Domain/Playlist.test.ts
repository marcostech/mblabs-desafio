import { Playlist } from '../../../app/domain/Playlist';
import { Tracks } from '../../../app/domain/Track';
import tracksData from '../../fixtures/tracks.json';


describe('Domain - Playlist', () => {
    it('Instance', () => {
        const track1 = new Tracks(tracksData);
        const track2 = new Tracks(tracksData);
        const tracksList: Tracks[] = [];
        tracksList.push(track1),
        tracksList.push(track2);
        const totalNumber = 100;
        const playlist = new Playlist({
            tracks: tracksList,
            totalTracks: totalNumber
        });
        expect(playlist).toBeInstanceOf(Playlist);
        expect(playlist.tracks[0].name).toBe(tracksData.name);
        expect(playlist.tracks[0].link).toBe(tracksData.link);
        expect(playlist.tracks[0].duration).toBe(tracksData.duration);
        expect(playlist.tracks[0].album.name).toBe(tracksData.album.name);        
        expect(playlist.tracks[0].album.link).toBe(tracksData.album.link);            
        expect(playlist.tracks[0].album.images).toBe(tracksData.album.images);
        expect(playlist.tracks[0].artists).toBe(tracksData.artists);
    });
});