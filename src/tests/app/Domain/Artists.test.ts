import { Artists } from '../../../app/domain/Artists';

describe('Domain - Artists', () => {
    it('Instance', () => {
        const mock = {
            name: 'name',
            link: 'http://link.com'
        };
        const artists = new Artists(mock);
        expect(artists).toBeInstanceOf(Artists);
        expect(artists.name).toBe('name');
        expect(artists.link).toBe('http://link.com');
    });
});