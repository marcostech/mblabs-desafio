import { Weather } from '../../../app/domain/Weather';

describe('Domain - Weather', () => {
    it('Instance', () => {
        const mock = {
            temperature: 10,
            city: 'cidade',
            time: new Date(Date.UTC(0, 0, 0, 0, 0, 0, 1000))
        };
        const weather = new Weather(mock);
        expect(weather).toBeInstanceOf(Weather);
        expect(weather.temperature).toBe(10);
        expect(weather.city).toBe('cidade');
        expect(weather.time).toStrictEqual(new Date(Date.UTC(0, 0, 0, 0, 0, 0, 1000)));
    });
});