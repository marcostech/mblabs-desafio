import { Tracks } from '../../../app/domain/Track';
import tracksData from '../../fixtures/tracks.json';


describe('Domain - Track', () => {
    it('Instance', () => {
        const tracks = new Tracks(tracksData);
        expect(tracks).toBeInstanceOf(Tracks);
        expect(tracks.name).toBe(tracksData.name);
        expect(tracks.link).toBe(tracksData.link);
        expect(tracks.duration).toBe(tracksData.duration);
        expect(tracks.album.name).toBe(tracksData.album.name);        
        expect(tracks.album.link).toBe(tracksData.album.link);            
        expect(tracks.album.images).toBe(tracksData.album.images);
        expect(tracks.artists).toBe(tracksData.artists);
    });
});