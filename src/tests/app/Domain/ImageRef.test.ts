import { ImageRef } from '../../../app/domain/ImageRef';


describe('Domain - ImageRef', () => {
    it('Instance -  complete', () => {
        const mock = {
            height: 101,
            width: 102,
            url: 'http://link.com'
        };
        const imageRef = new ImageRef(mock);
        expect(imageRef).toBeInstanceOf(ImageRef);
        expect(imageRef.height).toBe(101);
        expect(imageRef.width).toBe(102);
        expect(imageRef.url).toBe('http://link.com');
    });
    it('Instance - optional', () => {
        const mock2 = {
            url: 'http://link.com'
        };
        const imageRef2 = new ImageRef(mock2);
        expect(imageRef2).toBeInstanceOf(ImageRef);
        expect(imageRef2.height).toBeUndefined();
        expect(imageRef2.width).toBeUndefined();
        expect(imageRef2.url).toBe('http://link.com');
    });
});