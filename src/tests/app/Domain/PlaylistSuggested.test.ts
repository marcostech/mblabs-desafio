import { PlaylistDetails } from '../../../app/domain/PlaylistDetails';
import { PlaylistSuggested } from '../../../app/domain/PlaylistSuggested';
import { playlistDetails } from '../../fixtures/playlistSuggested.json';
import { playlistTracks } from '../../fixtures/playlistSuggested.json';

describe('Domain - PlaylistSugested', () => {
    it('Instance', () => {
        const playlistDetailsEnt = new PlaylistDetails(playlistDetails);
        const playlistSuggestedEnt = new PlaylistSuggested({
            playlistDetails: playlistDetailsEnt,
            playlistTracks: playlistTracks
        });
        expect(playlistSuggestedEnt).toBeInstanceOf(PlaylistSuggested);
        expect(playlistSuggestedEnt.playlistDetails.name).toBe(playlistDetails.name);
        expect(playlistSuggestedEnt.playlistDetails.url).toBe(playlistDetails.url);
        expect(playlistSuggestedEnt.playlistDetails.description).toBe(playlistDetails.description);
        expect(playlistSuggestedEnt.playlistDetails.playlistId).toBe(playlistDetails.playlistId);        
        expect(playlistSuggestedEnt.playlistDetails.image).toBe(playlistDetails.image);
        expect(playlistSuggestedEnt.playlistTracks.totalTracks).toBe(playlistTracks.totalTracks);
        expect(playlistSuggestedEnt.playlistTracks.tracks).toBe(playlistTracks.tracks);
    });
});