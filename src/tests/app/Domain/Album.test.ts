import { Album } from '../../../app/domain/Album';

describe('Domain - Album', () => {
    it('Instance', () => {
        const mock = {
            name: 'name',
            link: 'http://link.com',
            images: [ {url: 'http://link.com' } ]
        };
        const album = new Album(mock);
        expect(album).toBeInstanceOf(Album);
        expect(album.name).toBe('name');
        expect(album.link).toBe('http://link.com');
        expect(album.images[0].url).toBe('http://link.com');
    });
});