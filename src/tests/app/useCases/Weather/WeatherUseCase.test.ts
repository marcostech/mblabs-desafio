import { WeatherUseCase } from '../../../../app/useCase/Weather/WeatherUseCase';
import { WeatherIntegrationError } from '../../../../errors/BusinessExceptions/Weather/WeatherExceptions';
import { IMetadataDTO } from '../../../../infra/DTO/MetadataDTO';
import { WeatherAPIMock } from '../../../../providers/Weather/WeatherAPI/WeatherAPIMock';
import { LoggerMock } from '../../../../shared/utils/logger/loggerMock';
import locations from '../../../fixtures/locations.json';
import 'dotenv/config';

describe('UseCase - Weather', () => {
    const env = process.env;
    beforeEach(() => {
        jest.resetModules();
        process.env = { ...env };
    });

    afterEach(() => {
        process.env = env;
    });

    const metadataMock: IMetadataDTO = {
        traceid: '1',
    };
    const loggerMock = new LoggerMock();
    const wheaterProviderMock = new WeatherAPIMock(locations);
    const weatherUseCase = new WeatherUseCase(wheaterProviderMock, loggerMock);
    it('getClimate', async () => {
        const climate1 = await weatherUseCase.getClimate(25, metadataMock);
        expect(climate1).toBe('HOT');
        const climate2 = await weatherUseCase.getClimate(15, metadataMock);
        expect(climate2).toBe('WARM');
        const climate3 = await weatherUseCase.getClimate(5, metadataMock);
        expect(climate3).toBe('COLD');
    });    
    it('getTemperature - success', async () => {
        const temperature = await weatherUseCase.getTemperature('London', metadataMock);
        expect(temperature.temperature).toBe(7.0);
        expect(temperature.city).toBe('London');
    });
    it('getTemperature - error', async () => {
        const a = async () => {
           await weatherUseCase.getTemperature('cidade', metadataMock);
        };
        expect(a).rejects.toThrow(WeatherIntegrationError);
    });
    it('getTemperature envs', async () => {
        process.env.WARM_CELSIUS = '10';
        process.env.COLD_CELSIUS = '5';
        const climate1 = await weatherUseCase.getClimate(15, metadataMock);
        expect(climate1).toBe('HOT');
        const climate2 = await weatherUseCase.getClimate(8, metadataMock);
        expect(climate2).toBe('WARM');
        const climate3 = await weatherUseCase.getClimate(5, metadataMock);
        expect(climate3).toBe('COLD');
    });
});