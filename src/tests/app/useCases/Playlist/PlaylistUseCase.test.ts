
import 'dotenv/config';
import { LoggerMock } from '../../../../shared/utils/logger/loggerMock';
import { IMetadataDTO } from '../../../../infra/DTO/MetadataDTO';
import { SpotifyMock } from '../../../../providers/Music/Spotify/SpotifyMock';
import { PlaylistUseCase } from '../../../../app/useCase/Playlist/PlaylistUseCase';
import playlist from '../../../fixtures/playlist.json';
import { PlaylistClimateError, PlaylistIntegrationError } from '../../../../errors/BusinessExceptions/Playlist/PlaylistExceptions';

describe('UseCase - Playlist', () => {
    const env = process.env;
    beforeEach(() => {
        jest.resetModules();
        process.env = { ...env };
    });

    afterEach(() => {
        process.env = env;
    });

    const metadataMock: IMetadataDTO = {
        traceid: '1',
    };
    const loggerMock = new LoggerMock();
    const musicProviderMock = new SpotifyMock(playlist);
    const playlistUseCase = new PlaylistUseCase(musicProviderMock, loggerMock);
    it('getSuggestedPlaylistIdByTemp', async () => {
        const playlistId1 = await playlistUseCase.getSuggestedPlaylistIdByTemp('HOT', metadataMock);
        expect(playlistId1).toBe('37i9dQZF1DX8mBRYewE6or');
        const playlistId2 = await playlistUseCase.getSuggestedPlaylistIdByTemp('WARM', metadataMock);
        expect(playlistId2).toBe('37i9dQZF1DX8mBRYewE6or');
        const playlistId3 = await playlistUseCase.getSuggestedPlaylistIdByTemp('COLD', metadataMock);
        expect(playlistId3).toBe('37i9dQZF1DX8mBRYewE6or');
        expect(() => playlistUseCase.getSuggestedPlaylistIdByTemp('ANY', metadataMock)).toThrow(PlaylistClimateError);
    });
    it('getPlaylistDetailsById - success', async () => {
        const playlist = await playlistUseCase.getPlaylistDetailsById('37i9dQZF1DX8mBRYewE6or', metadataMock);
        expect(playlist.playlistId).toBe('37i9dQZF1DX8mBRYewE6or');
    });
    it('getPlaylistDetailsById - error', async () => {
        expect(() => playlistUseCase.getPlaylistDetailsById('123', metadataMock)).rejects.toThrow(PlaylistIntegrationError);
    });
    it('getPlaylistTracksById - success', async () => {
        const playlist = await playlistUseCase.getPlaylistTracksById('37i9dQZF1DX8mBRYewE6or', metadataMock);
        expect(playlist.totalTracks).toBe(95);
    });
    it('getPlaylistTracksById - error', async () => {
        expect(() => playlistUseCase.getPlaylistTracksById('123', metadataMock)).rejects.toThrow(PlaylistIntegrationError);
    });
    it('suggestPlaylist - success COLD', async () => {
        const playlist = await playlistUseCase.suggestPlaylist('COLD', metadataMock);
        expect(playlist.playlistDetails.playlistId).toBe('37i9dQZF1DX8mBRYewE6or');
    });
    it('getPlaylistPaginated - success', async () => {
        process.env.TRACKS_PER_PAGE = '15';
        const playlist = await playlistUseCase.getPlaylistPaginated('37i9dQZF1DX8mBRYewE6or', metadataMock);
        expect(playlist.totalTracks).toBe(95);
    });
    it('getPlaylistPaginated - success - no limit', async () => {
        process.env.TRACKS_PER_PAGE = undefined;
        const playlist = await playlistUseCase.getPlaylistPaginated('37i9dQZF1DX8mBRYewE6or', metadataMock);
        expect(playlist.totalTracks).toBe(95);
    });

});