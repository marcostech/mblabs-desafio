import { PlaylistClimateError, PlaylistIntegrationError } from '../../../../errors/BusinessExceptions/Playlist/PlaylistExceptions';

describe('Error - PlaylistException', () => {
    it('PLAYLIST_INTEGRATION_ERROR', () => {
        const a = () => {
            throw new PlaylistIntegrationError();
        };
        expect(a).toThrow(PlaylistIntegrationError);
        expect(a).toThrow('Playlist API Unknown Error');

    });
    it('PLAYLIST_CLIMATE_ERROR', () => {
        const b = () => {
            throw new PlaylistClimateError();
        };
        expect(b).toThrow(PlaylistClimateError);
        expect(b).toThrow('Playlist API Unknown Error');

    });
});