import { WeatherIntegrationError } from '../../../../errors/BusinessExceptions/Weather/WeatherExceptions';

describe('Error - WeatherException', () => {
    it('WEATHER_INTEGRATION_ERROR', () => {
        const a = () => {
            throw new WeatherIntegrationError();
        };
        expect(a).toThrow(WeatherIntegrationError);
        expect(a).toThrow('Weather API Unknown Error');

    });
});