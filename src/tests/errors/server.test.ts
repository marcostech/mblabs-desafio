import { ServerError, InvalidParameters } from '../../errors/serverExceptions';

describe('Error - serverException', () => {
    it('SERVER_ERROR', () => {
        const a = () => {
            throw new ServerError();
        };
        expect(a).toThrow(ServerError);
        expect(a).toThrow('Server Unknown Error');

    });
    it('INVALID_PARAMETERS', () => {
        const b = () => {
            throw new InvalidParameters();
        };
        expect(b).toThrow(InvalidParameters);
        expect(b).toThrow('Invalid parameters');

    });
});