import { Validator } from '../../../infra/middlewares/Validator';
import { Request, Response, NextFunction } from 'express';
import { LoggerMock } from '../../../shared/utils/logger/loggerMock';

describe('Validator', () => {
    const loggerMock = new LoggerMock();
    const mockRequest: Request = {
        headers: {
            traceid: '12345',
        },
    } as unknown as Request;

    const mockResponse: Response = {
      status: jest.fn(() => mockResponse),
      json: jest.fn(),
    } as unknown as Response;

    const mockNext: NextFunction = jest.fn();
  
    it('Validator - Success', () => {
      const schema = {
        key: {
          validate: jest.fn().mockReturnValue({ error: null }),
        },
      };  
      const middleware = Validator.validate(schema, loggerMock);
        middleware(mockRequest, mockResponse, mockNext);  
      expect(mockNext).toBeCalled();
    });
  
    it('Validator - Error', () => {
      const schema = {
        key: {
          validate: jest.fn().mockReturnValue({ error: new Error('Validation error') }),
        },
      };  
      const middleware = Validator.validate(schema, loggerMock);  
      middleware(mockRequest, mockResponse, mockNext);
      expect(mockResponse.status).toHaveBeenCalledWith(400);
    });
  });