
import { HttpResponse } from '../../../../infra/http/HttpResponse/HttpResponse';

describe('Domani - HttpResponse', () => {
    it('Status 200', () => {
        const statusOk = HttpResponse.ok('ok');
        expect(statusOk.statusCode).toBe(200);
        expect(statusOk.body).toBe('ok');
    });
    it('Status 400', () => {
        const statusBad = HttpResponse.badRequest({
                    name: 'errorBad',
                    message: 'errorBadMsg'
                }
        );
        expect(statusBad.statusCode).toBe(400);
        expect(statusBad.body).toStrictEqual({
            error: {
                erro: 'errorBad',
                message: 'errorBadMsg'
               }
           });
    });
    it('Status 500', () => {
        const statusServer = HttpResponse.serverError();
        expect(statusServer.statusCode).toBe(500);
        expect(statusServer.body).toStrictEqual({
             error: {
                 erro: 'SERVER_ERROR',
                 message: 'Server Unknown Error'
                }
            });
    });
});