
import 'dotenv/config';
import playlist from '../../fixtures/playlist.json';
import playlistNoMatch from '../../fixtures/playlistNoMatch.json';
import { PlaylistController } from '../../../infra/controller/PlaylistController';
import { WeatherAPIMock } from '../../../providers/Weather/WeatherAPI/WeatherAPIMock';
import locations from '../../fixtures/locations.json';
import { WeatherUseCase } from '../../../app/useCase/Weather/WeatherUseCase';
import { HttpRequestDTO } from '../../../infra/DTO/HttpRequestDTO';
import { LoggerMock } from '../../../shared/utils/logger/loggerMock';
import { SpotifyMock } from '../../../providers/Music/Spotify/SpotifyMock';
import { PlaylistUseCase } from '../../../app/useCase/Playlist/PlaylistUseCase';

describe('UseCase - Playlist', () => {
    const loggerMock = new LoggerMock();
    const musicProviderMock = new SpotifyMock(playlist);
    const weatherProviderMock = new WeatherAPIMock(locations);
    const weatherUseCase = new WeatherUseCase(weatherProviderMock, loggerMock);
    const playlistUseCase = new PlaylistUseCase(musicProviderMock, loggerMock);
    const playlistController = new PlaylistController(playlistUseCase, weatherUseCase, loggerMock);
    
    const musicProviderMock2 = new SpotifyMock(playlistNoMatch);    
    const playlistUseCase2 = new PlaylistUseCase(musicProviderMock2, loggerMock);    
    const playlistController2 = new PlaylistController(playlistUseCase2, weatherUseCase, loggerMock);
    it('getPlaylist - success', async () => {
        const request1: HttpRequestDTO = {
            query: {
                location: 'London'
            },
            headers: {
                traceid: '123'
            }
        };
        const response1 = await playlistController.getPlaylist(request1);
        expect(response1.statusCode).toBe(200);
    });
    it('getPlaylist - WEATHER_INTEGRATION_ERROR - 400', async () => {
        const request2: HttpRequestDTO = {
            query: {
                location: 'cidade'
            },
            headers: {
                traceid: '123'
            }
        };
        const response2 = await playlistController.getPlaylist(request2);
        expect(response2.statusCode).toBe(400);
    });
    it('getPlaylist - PLAYLIST_INTEGRATION_ERROR - 400', async () => {
        const request2: HttpRequestDTO = {
            query: {
                location: 'London'
            },
            headers: {
                traceid: '123'
            }
        };

        const response2 = await playlistController2.getPlaylist(request2);
        expect(response2.statusCode).toBe(400);
    });
    it('getPlaylist - PLAYLIST_CLIMATE_ERROR - 400', async () => {
        const request2: HttpRequestDTO = {
            query: {
                location: 'London'
            },
            headers: {
                traceid: '123'
            }
        };
        const spy = jest.spyOn(weatherUseCase, 'getClimate').mockImplementation(() => {return '';});
        const response2 = await playlistController2.getPlaylist(request2);
        spy.mockRestore();
        expect(response2.statusCode).toBe(400);
    });
    it('getPlaylist - error - 500', async () => {
        const request2: HttpRequestDTO = {
            headers: {
                traceid: '123'
            }
        };
        const response2 = await playlistController.getPlaylist(request2);
        expect(response2.statusCode).toBe(500);
    });
    it('getTracksPaginated - success', async () => {
        const request3: HttpRequestDTO = {
            query: {
                playlistId: '37i9dQZF1DX8mBRYewE6or'
            },
            headers: {
                traceid: '123'
            }
        };
        const response3 = await playlistController.getTracksPaginated(request3);
        expect(response3.statusCode).toBe(200);
    });
    it('getTracksPaginated - error - 400', async () => {
        const request3: HttpRequestDTO = {
            query: {
                playlistId: '37i9dQZF1DX6e81LupkkgG'
            },
            headers: {
                traceid: '123'
            }
        };
        const response3 = await playlistController.getTracksPaginated(request3);
        expect(response3.statusCode).toBe(400);
    });
    it('getTracksPaginated - error - 500', async () => {
        const request4: HttpRequestDTO = {
            headers: {
                traceid: '123'
            }
        };
        const response4 = await playlistController.getTracksPaginated(request4);
        expect(response4.statusCode).toBe(500);
    });

});