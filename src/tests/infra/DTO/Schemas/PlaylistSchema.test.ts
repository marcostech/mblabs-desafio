import { isSchema } from 'joi';
import { PlaylistSchema } from '../../../../infra/DTO/Schemas/PlaylistSchema';



describe('Domain - PlaylistSchema', () => {
    const schema = new PlaylistSchema();
    it('Instance - suggestionSchema', () => {
        const suggestionSchema = schema.suggestion();
        expect(suggestionSchema).toHaveProperty('query');
        expect(isSchema(suggestionSchema.query)).toBe(true);
    });
    it('Instance - PlaylistSchema', () => {
        const playlistSchema = schema.tracks();
        expect(playlistSchema).toHaveProperty('query');
        expect(isSchema(playlistSchema.query)).toBe(true);
    });
});