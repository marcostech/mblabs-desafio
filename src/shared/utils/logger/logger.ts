import winston from 'winston';
import { ILoggerMessage, ILoggerService } from '../../../app/providers/ILoggerService';
export class Logger implements ILoggerService {
    readonly loggerInstance: winston.Logger;
    constructor(){
        this.loggerInstance = winston.createLogger({
            level: 'debug',
            format: winston.format.json(),
            exitOnError: false,
            transports: [
              new winston.transports.Console({
                level: 'info',
                format: winston.format.combine(
                  winston.format.json()
                )
              }),
              new winston.transports.File({
                filename: 'filelog-info.log',
                level: 'info'
              }),
              new winston.transports.File({
                filename: 'filelog-error.log',
                level: 'error'
              })
            ]
        });
    }
    async log(level: string, message: ILoggerMessage): Promise<void> {
        await this.loggerInstance.log(level, message);
    }
}
export const logTypes = {
    error: 'error',
    info: 'info'
};