import { ILoggerService } from '../../../app/providers/ILoggerService';

export class LoggerMock implements ILoggerService {
    constructor(){}
    async log(): Promise<void> { }
}