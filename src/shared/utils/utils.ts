export function millisecondsToTime(timeInMs: number): string {
    const timeObject = new Date(Date.UTC(0, 0, 0, 0, 0, 0, timeInMs));
    const timeObjectPart = [
        String(timeObject.getUTCHours()).padStart(2, '0'),
        String(timeObject.getUTCMinutes()).padStart(2, '0'),
        String(timeObject.getUTCSeconds()).padStart(2, '0')
    ];
    const formatedString = `${timeObjectPart[0]}:${timeObjectPart[1]}:${timeObjectPart[2]}`;
    return formatedString;
}