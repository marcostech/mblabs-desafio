export interface HttpRequestDTO {
    params?: any,
    body?: any,
    headers?: any,
    query?: any,
}