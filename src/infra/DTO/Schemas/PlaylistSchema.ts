import Joi from 'joi';

export class PlaylistSchema {
    suggestion() {
        const schema = {
            query: Joi.object({
                location: Joi.string().required()
            })
        };
        return schema;
    }

    tracks() {
        const schema = {
            query: Joi.object({
                playlistId: Joi.string().required(),
                page: Joi.string()
            })
        };
        return schema;
    }
} 
