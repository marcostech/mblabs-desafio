export interface HttpResponseDTO {
    statusCode: number,
    body: any
}