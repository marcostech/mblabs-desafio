import { PlaylistUseCase } from '../../app/useCase/Playlist/PlaylistUseCase';
import { WeatherUseCase } from '../../app/useCase/Weather/WeatherUseCase';
import { Spotify } from '../../providers/Music/Spotify/Spotify';
import { WeatherAPI } from '../../providers/Weather/WeatherAPI/WeatherAPI';
import { Logger } from '../../shared/utils/logger/logger';
import { PlaylistController } from '../controller/PlaylistController';

const loggerService = new Logger();
const weatherProvider = new WeatherAPI(loggerService);
const weatherUseCase = new WeatherUseCase(weatherProvider, loggerService);
const musicProvider = new Spotify(loggerService);
const playlistUseCase = new PlaylistUseCase(musicProvider, loggerService);
const playlistController = new PlaylistController(playlistUseCase, weatherUseCase, loggerService);

export { playlistController, loggerService};
