import { ILoggerService } from '../../app/providers/ILoggerService';
import { IPlaylistUseCase } from '../../app/useCase/Playlist/IPlaylistUseCase';
import { IWeaterUseCase } from '../../app/useCase/Weather/IWeatherUseCase';
import { ORIGIN } from '../../shared/utils/logger/Origin';
import { logTypes } from '../../shared/utils/logger/logger';
import { HttpRequestDTO } from '../DTO/HttpRequestDTO';
import { HttpResponseDTO } from '../DTO/HttpResponseDTO';
import { IMetadataDTO } from '../DTO/MetadataDTO';
import { HttpResponse } from '../http/HttpResponse/HttpResponse';
import { IPlaylistController } from './IPlaylistController';

export class PlaylistController implements IPlaylistController {
    constructor(
        private playlistUseCase: IPlaylistUseCase,
        private weatherUseCase: IWeaterUseCase,
        private loggerService: ILoggerService
    ) { }
    async getPlaylist(request: HttpRequestDTO): Promise<HttpResponseDTO> {
        const metadata: IMetadataDTO = {
            traceid: request.headers.traceid
        };
        try {
            const weather = await this.weatherUseCase.getTemperature(request.query.location, metadata);
            const weatherClimate = this.weatherUseCase.getClimate(weather.temperature, metadata);
            const playlistSuggested = await this.playlistUseCase.suggestPlaylist(weatherClimate, metadata);
            return HttpResponse.ok(playlistSuggested);
        } catch (error: any) {
            this.loggerService.log(logTypes.error, {
                message: 'Erro no processamento da requisição',
                metadata,
                customData: error,
                origin: ORIGIN.PLAYLIST_SUGGESTION,
            });
            if (error?.name === 'PLAYLIST_INTEGRATION_ERROR') return HttpResponse.badRequest(error);
            if (error?.name === 'PLAYLIST_CLIMATE_ERROR') return HttpResponse.badRequest(error);
            if (error?.name === 'WEATHER_INTEGRATION_ERROR') return HttpResponse.badRequest(error);
            return HttpResponse.serverError();
        }
    }
    async getTracksPaginated(request: HttpRequestDTO): Promise<HttpResponseDTO> {
        const metadata: IMetadataDTO = {
            traceid: request.headers.traceid
        };
        try {
            const { page, playlistId } = request.query;
            const tracksPaginated = await this.playlistUseCase.getPlaylistPaginated(playlistId, metadata, page);
            return HttpResponse.ok(tracksPaginated);
        } catch (error: any) {
            this.loggerService.log(logTypes.error, {
                message: 'Erro no processamento da requisição',
                metadata,
                customData: error,
                origin: ORIGIN.PLAYLIST_PAGINATED_TRACKS,
            });
            if (error?.name === 'PLAYLIST_INTEGRATION_ERROR') return HttpResponse.badRequest(error);
            return HttpResponse.serverError();
        }
    }
}