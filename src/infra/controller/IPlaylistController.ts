import { HttpRequestDTO } from '../DTO/HttpRequestDTO';
import { HttpResponseDTO } from '../DTO/HttpResponseDTO';

export interface IPlaylistController {
    getPlaylist(request: HttpRequestDTO): Promise<HttpResponseDTO>
}