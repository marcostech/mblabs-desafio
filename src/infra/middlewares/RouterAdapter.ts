import { Request, Response } from 'express';
import { HttpRequestDTO } from '../DTO/HttpRequestDTO';
import { HttpResponseDTO } from '../DTO/HttpResponseDTO';

export class RouterAdapter {
    static adapt(controller: any, method: string) {
        return async (request: Request, response: Response) => {
            const httpRequest: HttpRequestDTO = {
                body: request.body,
                params: request.params,
                headers: request.headers,
                query: request.query,
            };

            const httpResponse: HttpResponseDTO = await controller[method](httpRequest);

            response.status(httpResponse.statusCode).json(httpResponse.body);
        };
    }
}