import { Request, Response, NextFunction } from 'express';
import { ILoggerService } from '../../app/providers/ILoggerService';
import { v4 } from 'uuid';
import { ORIGIN } from '../../shared/utils/logger/Origin';
import { logTypes } from '../../shared/utils/logger/logger';

export function Log(loggerService: ILoggerService){
    return (request: Request, response: Response, next: NextFunction) => {
        request.headers.traceid = v4();
        loggerService.log(logTypes.info, {
            message: 'Recieving Request',
            origin: ORIGIN.INCOMING_REQUEST,
            metadata: {
                traceid: request.headers.traceid
            },
            customData: {
                requestIp: request?.ip,
                requestMethod: request?.method,
                requestedEndpoint: request?.originalUrl
            }
        });
        next();
    };
}