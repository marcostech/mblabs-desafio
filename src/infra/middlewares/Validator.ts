import { NextFunction, Response } from 'express';
import { InvalidParameters } from '../../errors/serverExceptions';
import { HttpResponse } from '../http/HttpResponse/HttpResponse';
import { HttpStatusCode } from 'axios';
import { logTypes } from '../../shared/utils/logger/logger';
import { ORIGIN } from '../../shared/utils/logger/Origin';
import { ILoggerService } from '../../app/providers/ILoggerService';

export class Validator {
    static validate(schema: any, loggerService: ILoggerService) {
        return (request: any, response: Response, next: NextFunction) => {
            try {
                Object.keys(schema).map((k) => {
                    const { error } = schema[k].validate(request[k]); 
                    if (error) throw new InvalidParameters(error?.message);                
                  });
                next();
                
            } catch (error: any) {
                loggerService.log(logTypes.error, {
                    message: 'Erro na validação dos campos',
                    metadata: {
                        traceid: request?.headers?.traceid
                    },
                    origin: ORIGIN.VALIDATOR,
                    customData: error?.message
                });
                response.status(HttpStatusCode.BadRequest).json(HttpResponse.badRequest(error));
            }
        };       
    }
}